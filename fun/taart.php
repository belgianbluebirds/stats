<!DOCTYPE html>
<html>
    <head>
        <title>Top Categories of New Year's Resolution</title>
        <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
	<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
    </head>
 
    <?php
        $dataPoints = array(
            array("y" => 447.14, "name" => "Tmen Maus", "exploded" => true),
            array("y" => 222.80, "name" => "Frederik Herpol"),
            array("y" => 169.09, "name" => "Perez Gregory"),
            array("y" => 109.55, "name" => "Remy Noreillie"),
            array("y" => 102.54, "name" => "Thomas Galloo"),
            array("y" => 87.63, "name" => "Davy Caes"),
            array("y" => 80.15, "name" => "Mathieu Verdeyen"),
            array("y" => 76.04, "name" => "Nico Marcel"),
            array("y" => 57.30, "name" => "Kenney Debeaussaert"),
            array("y" => 35.45, "name" => "Dimitri Vanassche"),
            array("y" => 31.07, "name" => "Jens Polfliet"),
            array("y" => 21.12, "name" => "Kris Saey"),
            array("y" => 15.78, "name" => "Stijn Tanghe")   

        );
    ?>   
 
    <body>
        <div id="chartContainer"></div>
        <script type="text/javascript">
            $(function () {
                var chart = new CanvasJS.Chart("chartContainer",
                {
                    theme: "theme2",
                    title:{
                        text: "Kilometertaart"
                    },
                    exportFileName: "New Year Resolutions",
                    exportEnabled: true,
                    animationEnabled: true,		
                    data: [
                    {       
                        type: "pie",
                        showInLegend: false,
                        toolTipContent: "{name}: <strong>{y}km</strong>",
                        indexLabel: "{name}",
                        dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
                    }]
                });
                chart.render();
            });
        </script>
    </body>
 
</html>