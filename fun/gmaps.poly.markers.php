<?php 
error_reporting(E_ALL);
ini_set('display_errors', 'on');

require ("./config/settings.php");

if (!isset($_GET['w'])) { $_GET['w'] = date("W")-1;}

// Connect naar DB
$connection = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE);
//$connection = new mysqli($servername, $username, $password, $dbname);
if ($connection->connect_error) { die ("MySQL niet opgestart. Kalf. ". $connection->connect_error); }
// End connect DB
// END SETTINGS
	$sqlActivities = "SELECT aPolyline, aType, aName, aLat, aLong from activities WHERE weeknummer = " . $_GET["w"] . " AND aPolyline <> '' AND aType = 'Ride' OR aType = 'Run'"; 
	$result = $connection->query($sqlActivities);
?>

<html> 
<head> 
<title>Waar reden de Surplatsenaars overal naartoe?</title> 
<script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyCqT31FwUUq012zVh1I8Nu-1gUEboUT5Fw&libraries=geometry&amp;sensor=false"></script>
<style type="text/css"> 
#map {width:100%;height:100%;position: absolute; top: 0; left:0;}
</style> 

<script type='text/javascript'>
function initialize() {
    var myLatlng = new google.maps.LatLng(50.5039, 4.4699);
    var myOptions = {
        zoom: 8,
        center: myLatlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    var map = new google.maps.Map(document.getElementById("map"), myOptions);


   $kleur = NULL;
   $opacity = 0.25;
   //$teller = 1;
   //$numrows = $result->num_rows;

  /* echo "var locaties = [";
   while ($rowMarker = $result->fetch_assoc())
   {    
      
        echo "['" . $rowMarker["aName"] . "', " . $rowMarker["aLat"] . "," . $rowMarker["aLong"] . "," . $teller . "]";
        if ($teller < $numrows) { echo ", //" . $teller . "-" .$numrows . "\r\n"; }
  
        $teller++;
   }
      echo "];"; */


/* var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < locaties.length; i++) {  
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locaties[i][1], locaties[i][2]),
        map: map
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locaties[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    } */
      

    while ($row = $result->fetch_assoc())
    {     
        if ($row["aType"] == 'Ride')
        {
            $kleur = 'Red';
        }
        else {
            $kleur = 'Blue';
        }
        echo " var decodedPath = google.maps.geometry.encoding.decodePath('" .  $row["aPolyline"] . "');" . "\r\n"; 
        // echo " var decodedPath = google.maps.geometry.encoding.decodePath('" .   $row["aPolyline"] . "');" . "\r\n"; 
        
        echo "var decodedLevels = decodeLevels('BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB');" . "\r\n";

        echo "var setRegion = new google.maps.Polyline({" . "\r\n";
        echo "    path: decodedPath," . "\r\n"  . "\r\n";
        echo "    levels: decodedLevels," . "\r\n";
        echo "    strokeColor: '" . $kleur . "'," . "\r\n";
        echo "    strokeOpacity: " . $opacity . ",\r\n";
        echo "    strokeWeight: 5," . "\r\n";
        echo "    map: map" . "\r\n";
        echo "});" . "\r\n";
    }
    ?>

}
function decodeLevels(encodedLevelsString) {
    var decodedLevels = [];

    for (var i = 0; i < encodedLevelsString.length; ++i) {
        var level = encodedLevelsString.charCodeAt(i) - 63;
        decodedLevels.push(level);
    }
    return decodedLevels;
}

</script> 
</head> 
<body onload="initialize()"> 
<div id="map"></div>

</body> 