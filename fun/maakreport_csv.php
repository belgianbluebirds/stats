<?php
error_reporting(E_ALL);
ini_set('display_errors', 'on');
// SETTINGS
require ("config/settings.php");
// include "config/functions.php";
// is dit nog nodig om stats van een andere week te maken? Best de huidige week pakken zeker?
if (!isset($_GET['w'])) { $_GET['w'] = date("W")-1;}
// Zet de verstreken tijd mooi
function secondsToWords($seconds) {
    $ret = "";
    $days = intval(intval($seconds) / (3600*24));
    if($days> 0)     {
        $ret .= $days . "d ";
    }
    $hours = (intval($seconds) / 3600) % 24;
    if($hours > 0)    {
        $ret .= $hours ."u ";
    }
    $minutes = (intval($seconds) / 60) % 60;
    if($minutes > 0)    {
        $ret .= $minutes . "m ";
    }
    return $ret; 
}
// Einde verstreken tijd
function csv ($type, $jaar) {
	$tel=1; $totaalHoeveel = 0; $totaalAfstand = 0; $totaalHoogte = 0; $totaalTijd = 0;
	$connection = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE);
	if ($connection->connect_error) { die ("MySQL niet opgestart. Kalf. ". $connection->connect_error); }
	// End connect DB
	$sql = 	"SELECT leden.lFname,
					leden.lName,
					activities.aType,
					SUM(aDistance) as Oever,
					SUM(aMovingtime) as Oelang,
					SUM(aElevation) as OeOog,
					count(aID) as Oeveel
				FROM
					activities
				LEFT OUTER JOIN leden
				ON activities.athleetid = leden.id
				WHERE jaar =  $jaar 	AND weeknummer = " . $_GET["w"] . " AND activities.aType = '". $type ."'
				GROUP BY
					activities.Weeknummer,
					activities.athleetid
				ORDER BY Oever DESC";
	echo $sql;
	echo "<p>";
	$query = $connection->query($sql);
	// Maak file of updaten
	$file_stats = fopen('../stats/stats' . $type . '.csv', 'w');
	// Voeg headers toe aan array.
	$csvheader = array('Plaats','Voornaam', 'Naam', 'Aantal', 'Kilometers', 'Hoogtemeters', 'Tijd');
	// Haal 5de element uit Array (start te tellen vanaf 0)
	if ($type == 'Swim') { unset ($csvheader[5]); } 
	// Schrijf de headers weg naar de file
    fputcsv($file_stats, $csvheader);
	while($row = $query->fetch_assoc()) {
		$csvvelden = array($tel, $row["lFname"], $row["lName"], $row["Oeveel"], number_format($row["Oever"]/1000,2,',','.'), number_format($row["OeOog"],0,',','.'), gmdate("d:H:i:s",$row["Oelang"]) );
		// Haal 5de element uit Array (start te tellen vanaf 0)
		if ($type == 'Swim') { unset ($csvvelden[5]); } 
		// Vul de rest van de csv voor Janne
		fputcsv($file_stats, $csvvelden, ",");
		$totaalHoeveel = $totaalHoeveel + $row["Oeveel"];
		$totaalAfstand = $totaalAfstand + $row["Oever"];
		$totaalHoogte = $totaalHoogte + $row["OeOog"];
		$totaalTijd = $totaalTijd + $row["Oelang"];
		$tel++;
	}
	$csvtotals = array("Totaal", $totaalHoeveel, number_format($totaalAfstand/1000,2,',','.') . "km", number_format($totaalHoogte,0,',','.') . "m", SecondsToWords($totaalTijd));
	fputcsv($file_stats, $csvtotals);
}
csv('Ride',        2017);
csv('VirtualRide', 2017);
csv('Run',         2017);
csv('Swim',        2017);
// csv('Hiking', 2017) kan ook bvb
?>		