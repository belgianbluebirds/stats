<html>
<?php
error_reporting(E_ALL);
ini_set('display_errors', 'on');

require ("config/settings.php");

// SETTINGS
//Leden
$curlMembers = curl_init();
// curl_setopt ($curlMembers, CURLOPT_URL, "https://www.strava.com/api/v3/clubs/153253/members?access_token=98e3a0823272720e2754566d248f2896212316ec&per_page=200");
curl_setopt ($curlMembers, CURLOPT_URL, "https://www.strava.com/api/v3/clubs/153253/members?access_token=104e0a4a0f4c598bfa62446c6246f8f8213166fa&per_page=200");
curl_setopt($curlMembers, CURLOPT_RETURNTRANSFER, 1);
$resultMembers = curl_exec ($curlMembers);
curl_close ($curlMembers);
$JSONObj = json_decode($resultMembers); 


// Activiteiten
$curlActivities = curl_init();
//curl_setopt ($curlActivities, CURLOPT_URL, "https://www.strava.com/api/v3/clubs/153253/activities?access_token=98e3a0823272720e2754566d248f2896212316ec&per_page=200");
curl_setopt ($curlActivities, CURLOPT_URL, "https://www.strava.com/api/v3/clubs/153253/activities?access_token=104e0a4a0f4c598bfa62446c6246f8f8213166fa&per_page=200");
curl_setopt($curlActivities, CURLOPT_RETURNTRANSFER, 1);
$result = curl_exec ($curlActivities);
curl_close ($curlActivities); 
$JSONObjAct = json_decode($result);

// Connect naar DB
$connection = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE);
//$connection = new mysqli($servername, $username, $password, $dbname);
if ($connection->connect_error) { die ("MySQL niet opgestart. Kalf. ". $connection->connect_error); }
// End connect DB
// END SETTINGS

// Leden in db djoefen.
foreach ($JSONObj as $key => $value) {
	// Eerst checken of "the member" al in de users tabel zit.
	// if not, awel, steekt ze er in.
	if ($result = $connection->query("SELECT id FROM leden WHERE id = " . $value->id)->num_rows == 0) {
		$sqlMembers = "INSERT INTO leden (id, lFname, lName, lStad, lProvincie, lSex) VALUES (" . $value->id . ",'" . $value->firstname . "','" . $value->lastname . "','" . $value->city . "','" . $value->state . "','" . $value->sex . "')";
		$connection->query($sqlMembers);
		echo $value->firstname . " " . $value->lastname . "<br>";
	}
}
// End Foreach ledenlijst

// foreach laatste 200 activiteiten leden BelgianBluebirds
// Avg speed in m/s. Vermenigvuldigen met 3,6
foreach ($JSONObjAct as $keyAct => $valueAct) {
	// Checken of die activity er al in zit
	// if not, djoeft ze er in.
	if ($result = $connection->query("SELECT aID FROM activities WHERE aid = " . $valueAct->id)->num_rows == 0) {
		// Haal de weeknummer op
		$datumAct = DateTime::createFromFormat('Y-m-d\TH:i:s\Z', $valueAct->start_date); 
		$weeknr = $datumAct->format('W');
		$avgWatt = NULL;
		$deviceWatt = NULL;
		$maxHR = NULL;
		$lat = NULL;
		$lon = NULL;
		// Watt toevoegen
		// Property enkel beschikbaar voor Rides
		if ($valueAct->type == 'Ride') {
			if (isset($valueAct->average_watts)) { $avgWatt = $valueAct->average_watts; }
			else { $avgWatt = 0; };
			//if (isset($valueAct->device_watts)) { $deviceWatt = $valueAct->device_watts; }
			//else { $deviceWatt = 0; };
			//if ($valueAct->average_watts = NULL) { $avgWatt = 0; }
			//else { $avgWatt = $valueAct->average_watts; }
			// $avgWatt = $valueAct->average_watts;
			if ($valueAct->device_watts) { 	$deviceWatt = $valueAct->device_watts; }
			else { $deviceWatt = 0;	}
		} else {		 
			$avgWatt = 'NULL';
			$deviceWatt = 'NULL';  
		}
		if (isset($valueAct->max_heartrate)) { $maxHR = $valueAct->max_heartrate; }
		else { $maxHR = 'NULL'; };
		if (isset ($valueAct->start_latitude)) { $lat = $valueAct->start_latitude; $lon = $valueAct->start_longitude;} else { $lat = 'NULL'; $lon = 'NULL';}
		// Eind weeknummer
		$sqlActivities = "INSERT INTO activities (athleetid, aID, aName, aDate, aDistance, aElevation, aType, aMovingtime, aElapsedtime, Weeknummer, aAvgSpeed, aGearID, aMaxSpeed, aAvgWatt, aDeviceWatts, aLat, aLong, aMaxHR, jaar) VALUES (" . $valueAct->athlete->id . ",'" . $valueAct->id . "','" . addslashes($valueAct->name) . "','" . $valueAct->start_date . "'," . $valueAct->distance . "," . $valueAct->total_elevation_gain . ",'" . $valueAct->type . "'," . $valueAct->moving_time .  "," . $valueAct->elapsed_time . "," . $weeknr . "," . $valueAct->average_speed . ",'" . $valueAct->gear_id . "'," . $valueAct->max_speed . "," . $avgWatt . "," . $deviceWatt . ", "  . $lat . ", " . $lon . ", " . $maxHR . ",2017)"; // "," . $valueAct->start_latlng .
		echo $sqlActivities . "<br>";
		echo $valueAct->athlete->firstname . " " . $valueAct->athlete->lastname . " - " . addSlashes($valueAct->name). "<br>";
		//echo $sqlActivities . "<br>";
		$connection->query($sqlActivities);
	} 
	// ELSE EEN UPDATE! Alhoewel, titel boeit niet.
}
 echo "Databases populated.<br>";
?>
</html>