<html>
<body>
<?php
error_reporting(E_ALL);
ini_set('display_errors', 'on');
require ("config/settings.php");

$connection = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE);
if ($connection->connect_error) { die ("MySQL niet opgestart. Kalf. ". $connection->connect_error); }
$tel=1;
// End connect DB
$sql = 	"SELECT id, lFname, lName FROM leden";
$test = $connection->query($sql);
// Loop user
while($row = $test->fetch_assoc()) {
        $activitiesFile = file_get_contents('https://www.strava.com/api/v3/athletes/'. $row["id"] . '/routes/?access_token=104e0a4a0f4c598bfa62446c6246f8f8213166fa&type=ride&per_page=200');
        // echo "https://www.strava.com/api/v3/athletes/". $row["id"] . "/routes/?access_token=104e0a4a0f4c598bfa62446c6246f8f8213166fa&per_page=200<br>";
        $JSONObjAct = json_decode($activitiesFile);
        echo "<b>Routes van " . $row["lFname"] . " " . $row["lName"] . "</b><br>";
        // Loop routes van die user
        foreach ($JSONObjAct as $keyAct => $valueRoute) {
            if ($row["id"] == $valueRoute->athlete->id) {
                echo "<p>";
                echo "Titel: " . $valueRoute->name . "<br>";
                echo "Lengte: " .  number_format($valueRoute->distance/1000,2) . "km"; echo "<br>Hoogtemeters: " . number_format($valueRoute->elevation_gain,0) . "m<br>";
                echo "<a href='https://www.strava.com/routes/". $valueRoute->id . "'>link naar Strava</a> - "; 
                echo "<a href='https://www.strava.com/routes/". $valueRoute->id . "/export_gpx'>Opslaan als GPX</a><br>";
                echo "</p>";
                // echo "https://www.strava.com/api/v3/routes/" . $valueRoute->id . "?access_token=104e0a4a0f4c598bfa62446c6246f8f8213166fa<br>";
            }
        }
         echo "<hr>";
        // var_dump($JSONObjAct);
}
?>
</body>
</html>