﻿<?php
// Zet de verstreken tijd mooi
function secondsToWords($seconds) {
    $ret = "";
    $days = intval(intval($seconds) / (3600*24));
    if($days> 0)     {
        $ret .= $days . "d ";
    }
    $hours = (intval($seconds) / 3600) % 24;
    if($hours > 0)    {
        $ret .= $hours ."u ";
    }
    $minutes = (intval($seconds) / 60) % 60;
    if($minutes > 0)    {
        $ret .= $minutes . "m ";
    }
    return $ret; 
}
// Einde verstreken tijd

// Geef begin- en einddatum van die week
/* function getStartAndEndDate($week, $year) {
  $dto = new DateTime();
  $dto->setISODate($year, $week);
  $ret['week_start'] = $dto->format('d-m-Y');
  $dto->modify('+6 days');
  $ret['week_end'] = $dto->format('d-m-Y');
  return $ret;
} */
// Eind begin- en einddatum

function CijferNaarMaand ($maand) { 
	$maandnaam = '';
	switch($maand) {
		case 1: $maandnaam = "januari"; break;
		case 2: $maandnaam = "februari"; break;
		case 3: $maandnaam = "maart"; break;
		case 4: $maandnaam = "april"; break;
		case 5: $maandnaam = "mei"; break;
		case 6: $maandnaam = "juni"; break;
		case 7: $maandnaam = "juli"; break;
		case 8: $maandnaam = "augustus"; break;
		case 9: $maandnaam = "september"; break;
		case 10: $maandnaam = "oktober"; break;
		case 11: $maandnaam = "november"; break;
		case 12: $maandnaam = "december"; break;
	}
	return $maandnaam;
}
// Maak queries dynamisch
function maakTabel ($week, $type) {
	// Connect naar DB
	$connection = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE);
	if ($connection->connect_error) { die ("MySQL niet opgestart. Kalf. ". $connection->connect_error); }
	$tel=1;
	// End connect DB
	$sql = 	"SELECT leden.lFname,
					leden.lName,
					activities.aType,
					SUM(aDistance) as Oever,
					SUM(aMovingtime) as Oelang,
					SUM(aElevation) as OeOog,
					count(aID) as Oeveel,
					SUM(aPRs) as OeGoed,
					MONTH(activities.aDate) as Maand
				FROM
					activities
				LEFT OUTER JOIN leden
				ON activities.athleetid = leden.id
				WHERE jaar = 2017	AND MONTH(activities.aDate) = " . $_GET["w"] . " AND activities.aType like '". $type ."'
				GROUP BY
					MAAND,
					activities.athleetid
				ORDER BY Oever DESC";
	$test = $connection->query($sql);
?>
	<!-- div class=datagrid -->
		<table border="0" class="responsive">
			<thead> 
				<th align="center"><img src="https://wtcsurplatse.be/assets/wie.png" height="6%" title="Wie?"></th>
				<th align="center"><img src="https://wtcsurplatse.be/assets/aantal.png" height="6%" title="Aantal ritten"></th>
				<th align="center"><img src="https://wtcsurplatse.be/assets/map.png" height="6%" title="Totaal aantal kilometers"></th>
				<th align="center"><img src="https://wtcsurplatse.be/assets/klim.png" height="6%" title="Totaal aantal hoogtemeters"></th> 
				<!-- th align="center"><img src="img\clock.png" title="Totale tijd op de fiets"></th -->
				<th align="center"><img src="https://wtcsurplatse.be/assets/trophy.png" height="6%" title="Aantal PRs"></th>
				<th align="center"><img src="https://wtcsurplatse.be/assets/decathlon.png" width="50%" title="Hoeveel bongs van Decathlon?"></th>
				<th align="center">Nog kms</th>
			</thead>
			<tbody>
	<?php
		$totaalAfstand = 0;
		$totaalHoeveel = 0;
		$totaalHoogte = 0;
		$totaalTijd = 0;
		$totaalDecathlon = 0;
		$totaalPRs = 0;
		$maanddtijd = 0;
		$tijd = 0;
 		while($row = $test->fetch_assoc()) { // Start While MaakStats
	?>	 
			<tr>
				<td><?php echo $tel . ". " . $row["lFname"] . " " . $row["lName"]; ?> </td>
				<td><?php echo $row["Oeveel"];?></td>
				<td><?php echo number_format($row["Oever"]/1000,2,',','.');?>km</td>
				<td><?php echo number_format($row["OeOog"],0,',','.');?>m</td> 
				<!-- td><?php echo gmdate("d H:i:s",$row["Oelang"]);?></td -->
				<td><?php echo $row["OeGoed"];?> PRs</td> 
				<td><?php if (floor(($row["Oever"]/1000)/400) >= 1) { echo floor(($row["Oever"]/1000)/400);} else { echo "ZERO"; }?> bong(s)</td>
				<td><?php echo number_format((floor(($row["Oever"]/1000)/400)+1)*400 - $row["Oever"]/1000,2,',','.'); ?>km voor <b><?php echo (floor(($row["Oever"]/1000)/400)+1);?></b></td>
			</tr>
	<?php
		// Totalen
		$totaalHoeveel = $totaalHoeveel + $row["Oeveel"];
		$totaalAfstand = $totaalAfstand + $row["Oever"];
		$totaalHoogte = $totaalHoogte + $row["OeOog"];
		$totaalTijd = $totaalTijd + $row["Oelang"];
		$totaalPRs = $totaalPRs + $row["OeGoed"];
		$totaalDecathlon = $totaalDecathlon + floor(($row["Oever"]/1000)/400);
		$tel++;
		} // Einde While MaakStats
	?>
			</tbody>
			<tfoot>
				<td><b>Totaal</b></td>
				<td><b><?php echo $totaalHoeveel;?></b></td>
				<td><b><?php echo number_format($totaalAfstand/1000,2,',','.');?>km</b></td>
				<td><b><?php echo number_format($totaalHoogte,0,',','.');?>m</b></td> 
				<!-- td><b><?php echo secondsToWords($totaalTijd);?></b></td -->
				<td><b><?php echo $totaalPRs;?></b></td>
				<td><b><?php echo $totaalDecathlon;?> (<?php echo $totaalDecathlon*6 . "€";?>)</b></td>
				<td>.</td>
			</tfoot>
		</table>
	<?php
	mysqli_close($connection);
}
// Eind dynamische queries

// Custom stats
function maakSnelleStats ($week, $type, $order, $titel) {
	// Connect naar DB
	$connection = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE);
	if ($connection->connect_error) { die ("MySQL niet opgestart. Kalf. ". $connection->connect_error); }
	$sqlBerg = "";
	$sqlCommuteIn ="";
	$sqlGroup = "";	
	$sqlWatt = "";
	$sqlCommute = "";
	$sqlFotoots = "";
	switch($order) {
		case "snel":
			$sqlOrder = "activities.aAvgSpeed";
			break;
		case "lang":
			$sqlOrder = "activities.aMovingtime";
			break;
		case "avg":
			$sqlOrder = "activities.aAvgSpeed";
			break;
		case "turbo":
			$sqlOrder = "activities.aMaxSpeed";
			break;
		case "berg":
			$sqlBerg = "SUM(aElevation) as OeOog,";
			$sqlGroup = "GROUP BY Maand, activities.aType, activities.athleetid";
			$sqlOrder = "OeOog";
			break;
		case "lanter":
			$sqlOrder = "lanterfanten";
			break;
		case "watt":
			$sqlOrder = "activities.aAvgWatt";
			$sqlWatt = "AND activities.aDeviceWatts = 1";
			break;
		case "maxhr":
			$sqlOrder = "activities.aMaxHR";
			break;
		case "fotograaf":
			$sqlFotoots = ",SUM(activities.aFotos) as OeveelFotoots";
			$sqlGroup = "GROUP BY Maand, activities.athleetid";
			$sqlOrder = "OeveelFotoots";
			break;
		default:
	} 
	$sql = "	SELECT 	leden.lFname,
					leden.lName,
					activities.aName,
					activities.aID,
					activities.aDistance," 
					. $sqlBerg . "
					activities.aMovingtime,
					activities.aElapsedtime,
					aAvgSpeed*3.6 as speed,
					aMaxSpeed*3.6 as Oerap,
					activities.aAvgWatt,
					MONTH(activities.aDate) as Maand,
					activities.aMaxHR"
					. $sqlFotoots   
					. $sqlCommuteIn . "
				FROM
						activities
				LEFT OUTER JOIN leden
				ON activities.athleetid = leden.id
				WHERE 1 = 1 AND jaar = 2017 AND MONTH(activities.aDate) =  " . $week . " AND activities.aType = '" . $type. "'" . $sqlWatt .	" " . $sqlGroup . " " . $sqlCommute . "
				ORDER BY " . $sqlOrder. " DESC";
	$test = $connection->query($sql);	
	?>
		<div class="datagrid">
				<table style="width:100%">
					<thead> 
						<th colspan="2" align="center">
						<?php echo $titel;?> </th>
					</thead>
				<tbody>
				<?php 
				$row = $test->fetch_assoc();
				echo "<tr><td>" . $row['lFname'] . " ". $row['lName'] ;
				switch ($titel) {
				case "Locomotief":
						echo " knalde aan " . number_format($row['speed'],1,',','.') . "km/u avg op "  . number_format($row['aDistance']/1000,1,',','.') . "km.";
						echo "<br><a href=\"https://www.strava.com/activities/" . $row['aID'] . "\" target='_blank'>Bekijk op Strava</a> als je het niet gelooft.";
						break;
					case "Zeemveltester":
						echo " zat " . gmdate("H:i:s",$row['aMovingtime']) ." op de fiets.";
						echo "<br><a href=\"https://www.strava.com/activities/" . $row['aID'] . "\" target='_blank'>Bekijk op Strava</a> als je het niet gelooft.";
						break;
					case "Berggeit":
						echo " klom een totaal van  " . number_format($row['OeOog'],0,',','.') . "m.";
						break;
					case "Turbo":
						echo "'s turbo bolde aan " . number_format($row['Oerap'],1) . "km/u.";
						echo "<br><a href=\"https://www.strava.com/activities/" . $row['aID'] . "\" target='_blank'>Bekijk op Strava</a> als je het niet gelooft.";
						break;
					case "Krachtbeest":
						echo " reed " . $row['aAvgWatt'] . "w avg.";
						echo "<br><a href=\"https://www.strava.com/activities/" . $row['aID'] . "\" target='_blank'>Bekijk op Strava</a> als je het niet gelooft.";
						break;
					case "Liefdevolste":
						echo "'s ❤️‍ klopte max " . $row['aMaxHR'] . "x voor jou.";
						echo "<br><a href=\"https://www.strava.com/activities/" . $row['aID'] . "\" target='_blank'>Bekijk op Strava</a> als je het niet gelooft.";
						break;
					case "Fotograaf":
						echo "  heeft " . $row['OeveelFotoots'] .  " 📷's getrokken.";
						break;
					/*case "Decathlon plunderaar":
						echo "  heeft " . $row['OeveelFotoots'] .  " 📷's getrokken.";
						break;*/	
					default:
						echo "Configureren kalf";
					}
					?>
						</td>
					</tr>
				</table>
			</div>
	<?php		
		mysqli_close($connection);
}
// Maakt een schoon taartje
function maakChart($type, $nr)
{
	switch ($type){
		case "kms":
			$sqlTaart = "TRUNCATE(SUM(aDistance)/1000,2)";
			$titel = "Kilometers sinds 11/07/2017";
			$eenheid = "km";
			break;
		case "bollekes":
			$sqlTaart = "TRUNCATE(SUM(aElevation),2)";
			$titel = "Hoogtemeters sinds 11/07/2017";
			$eenheid = "m";
			break;
		default:
	}

	$connection = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE);
	if ($connection->connect_error) { die ("MySQL niet opgestart. Kalf. ". $connection->connect_error); }
	$dataPoints = array();
	$sqlArray = "SELECT $sqlTaart as y, CONCAT(leden.lFname,' ',leden.lName) as name
				FROM
					activities
				LEFT OUTER JOIN leden
				ON activities.athleetid = leden.id
				WHERE jaar = 2017 
				GROUP BY
					activities.athleetid
				ORDER BY y DESC";
	// echo $sqlArray;
	$testArray = $connection->query($sqlArray);	
	while($rowChart = $testArray->fetch_assoc()) { // Start While MaakChart
		array_push($dataPoints, $rowChart);
	}
?>	
		<div id="chartContainer<?php echo $nr;?>" style="left: 0px; width: 100%; height: 200px; display: inline-block;"></div></br></br>
        		<script type="text/javascript">
           		$(function () {
                var chart = new CanvasJS.Chart("chartContainer<?php echo $nr;?>",
                {
                    theme: "theme2",
                    title:{
                        text: "<?php echo $titel; ?>"
                    },
                    exportEnabled: false,
                    animationEnabled: true,
                    data: [
                    {       
                        indexLabelFontSize: 9,
						type: "pie",
                        showInLegend: false,
                        toolTipContent: "{name}: <strong>{y}<?php echo $eenheid;?></strong>",
                        indexLabel: "{name}",
						dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
              	      }]
              	  });
              	  chart.render();
           		 });
        		</script> 
<?php	
	mysqli_close($connection);		
}	

function toonlaatsteritten($hoeveel)
{
// Connect naar DB
	$connection = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE);
	if ($connection->connect_error) { die ("MySQL niet opgestart. Kalf. ". $connection->connect_error); }
	$tel=1;
	// End connect DB
	$sql = 	"SELECT leden.lFname,
					leden.lName,
					activities.aType,
					activities.aName,
					activities.aID,
					activities.aDistance,
					activities.aElevation,
					activities.aMovingtime,
					activities.aDate
				FROM
					activities
				LEFT OUTER JOIN leden
				ON activities.athleetid = leden.id
				ORDER BY activities.aDate DESC
				LIMIT $hoeveel";
	$test = $connection->query($sql);
	echo "<table>\n";
	echo "<th colspan=\"2\" align=\"center\">Laatste $hoeveel ritten</th>\n";
	while($row = $test->fetch_assoc()) { // Start While MaakStats
	 	echo "<tr>\n";
			echo "<td>" . $row["lFname"] . " reed ";
	 		echo "<b>" . number_format($row["aDistance"]/1000,2,',','.'). "</b>km in <b>". gmdate("H:i:s",$row['aMovingtime']) . "</b> met <b>" . number_format($row["aElevation"],0,',','.') .  "m</b>.</td>\n";
			echo "<td><a href=\"https://www.strava.com/activities/" . $row['aID'] . "\" target='_blank'><img height=\"50%\" src=\"https://wtcsurplatse.be/assets/kudos2.png\"></a></td>\n";
		echo "</tr>\n";
	 }
	echo "</table>\n";
}
// Einde custom stats
//mysql_affected_rows
// Drukste dag: SELECT count(aid), date(adate) FROM `activities` group by date(adate) ORDER BY `adate` ASC
// Drukste week: SELECT count(aid) as oeveel, weeknummer, jaar from activities group by jaar, weeknummer order by oeveel desc
?>