<?php
	ob_start();
?>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" href="https://wtcsurplatse.be/assets/fav/favicon.ico" type="image/x-icon">
	<link rel="icon" href="https://wtcsurplatse.be/assets/fav/favicon.ico" type="image/x-icon">
	<link rel="apple-touch-icon" sizes="57x57" href="https://wtcsurplatse.be/assets/https://wtcsurplatse.be/assets/fav/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="https://wtcsurplatse.be/assets/https://wtcsurplatse.be/assets/fav/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="https://wtcsurplatse.be/assets/fav/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="https://wtcsurplatse.be/assets/fav/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="https://wtcsurplatse.be/assets/fav/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="https://wtcsurplatse.be/assets/fav/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="https://wtcsurplatse.be/assets/fav/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="https://wtcsurplatse.be/assets/fav/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="https://wtcsurplatse.be/assets/fav/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192" href="https://wtcsurplatse.be/assets/fav/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="https://wtcsurplatse.be/assets/fav/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="https://wtcsurplatse.be/assets/fav/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="https://wtcsurplatse.be/assets/fav/favicon-16x16.png">
	<link rel="manifest" href="https://wtcsurplatse.be/assets/fav/manifest.json">
	<!-- Chrome, Firefox OS & Opera -->
	<meta name="theme-color" content="#6E6A66">
	<!-- Windows Phone -->
	<meta name="msapplication-navbutton-color" content="#6E6A66">
	<!-- iOS Safari -->
	<meta name="apple-mobile-web-app-status-bar-style" content="#6E6A66">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="https://wtcsurplatse.be/assets/fav/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<title>WTC SurPlatse stats</title>
	<link rel="stylesheet" type="text/css" media="screen" href="https://wtcsurplatse.be/assets/css/surplatse.css" />
	<link rel="stylesheet" type="text/css" media="screen" href="https://wtcsurplatse.be/assets/css/responsive-tables.css" />
	<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
	<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>
	<script src="https://wtcsurplatse.be/assets/css/responsive-tables.js"></script>
	<script>
	var takeScreenShot = function() {
    html2canvas(document.getElementById("OuterDiv"), {
        onrendered: function (canvas) {
			document.body.appendChild(canvas);
            /* var tempcanvas=document.createElement('canvas');
            tempcanvas.width=375;
            tempcanvas.height=667;
            var context=tempcanvas.getContext('2d');
            context.drawImage(canvas,112,0,288,200,0,0,375,667); */
            var link=document.createElement("a");
            link.href=canvas.toDataURL('image/jpg');   //function blocks CORS
            link.download = 'screenshot'+ Math.random() + '.jpg';
            link.click(); 
        	}
    	});
	}  </script>
</head>
<body>
	<?php
	// SETTINGS
	require ("cron/config/settings.php");
	include "cron/config/functions.php";
	// if (!isset($_GET['w'])) { $_GET['w'] = date("n")-1;}
	if (!isset($_GET['w'])) { $_GET['w'] = date("n"); }
	
	// $weekDaterange = getStartAndEndDate($_GET['w'], '2017');
?>
		<div id="OuterDiv">
			<!-- Start OuterDiv -->
			<div class="blijfstaan">
				<img src="https://wtcsurplatse.be/assets/header.png" title="SUR MOTHERFUCKING PLATSE" class="responsive-image" align="center"><br>
				Powered by Strava & onze benen
			</div>
			<span style="display: block; font-size: 20px;border: 0px solid #ffffff;text-align: center;">Stats voor <?php echo CijferNaarMaand($_GET['w']);?></span>
			<table border="0" width="100%" cellpadding="0" style="border-radius: 25px;">
				<!-- tr align="center" valign="top" -->
				<tr valign="top">
					<td>
						<?php maakTabel ($_GET['w'], 'ride'); // Maak overzicht van de coureurs ?>
					</td>
				</tr>
				<tr>
					<td>
						<div class="koers">
							<?php 	
								maakSnelleStats($_GET['w'], 'ride', 'berg', 'Berggeit');
								maakSnelleStats($_GET['w'], 'ride', 'turbo', 'Turbo');	
								// maakSnelleStats($_GET['w'], 'ride', 'watt', 'Krachtbeest');	
								maakSnelleStats($_GET['w'], 'ride', 'avg', 'Locomotief');	
								maakSnelleStats($_GET['w'], 'ride', 'lang', 'Zeemveltester');	
								maakSnelleStats($_GET['w'], 'ride', 'maxhr', 'Liefdevolste');	
								maakSnelleStats($_GET['w'], 'ride', 'fotograaf', 'Fotograaf');
								toonlaatsteritten(10); 
							?>
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<span style="display: block; font-size: 25px; text-align:center;"><a href="heatmap.html" target="_blank" >Heatmap voor <?php echo CijferNaarMaand($_GET['w']);?></a></span>
					</td>
				</tr>

			</table>
			<?php maakChart("kms", 1); ?>
			<?php maakChart("bollekes", 2); ?>
			&copy;Jay 2017 - 		<button href="#" onclick="takeScreenShot()">Screenshot</button>
		</div>
		<!-- End OuterDiv -->
		<script>
			(function (i, s, o, g, r, a, m) {
				i['GoogleAnalyticsObject'] = r;
				i[r] = i[r] || function () {
					(i[r].q = i[r].q || []).push(arguments)
				}, i[r].l = 1 * new Date();
				a = s.createElement(o),
					m = s.getElementsByTagName(o)[0];
				a.async = 1;
				a.src = g;
				m.parentNode.insertBefore(a, m)
			})(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

			ga('create', 'UA-103774003-1', 'auto');
			ga('send', 'pageview');
		</script>

</html>
<!-- End HTML -->
<?php // write ob_start() naar file
	file_put_contents('../index_stats.html',ob_get_contents()); 

	/* $file = "index_stats.html";
	$remote_file = "index.htm";
	$connectie = ftp_connect("surplatsestats.dx.am");
	$login_result = ftp_login($connectie, '2402773_surplatse', 'IkBenVanWTCSurplatse8500!');
	ftp_chdir($connectie, "surplatsestats.dx.am");
	ftp_pasv($connectie, true);
	if (ftp_put($connectie, $remote_file, $file, FTP_ASCII)) {
		echo "OK\n";
	} else {
 		echo "NOK\n";	
	}
	ftp_close($connectie); */
?>
