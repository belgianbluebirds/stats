<?php
// E_ALL toont de echte fouten ipv "Error 500"
error_reporting(E_ALL);
ini_set('display_errors', 'on');
// settings.php bevat de code voor connectie met de databank (login/passwoord)
require ("config/settings.php");

// SETTINGS
//Leden
/*$curlMembers = curl_init();
curl_setopt ($curlMembers, CURLOPT_URL, "https://www.strava.com/api/v3/clubs/153253/members?access_token=104e0a4a0f4c598bfa62446c6246f8f8213166fa&per_page=200");
curl_setopt($curlMembers, CURLOPT_RETURNTRANSFER, 1);
$resultMembers = curl_exec ($curlMembers);
curl_close ($curlMembers);
$JSONObj = json_decode($resultMembers, JSON_PRETTY_PRINT); 
var_dump($JSONObj);*/

// https://www.strava.com/api/v3/clubs/193861/members?access_token=104e0a4a0f4c598bfa62446c6246f8f8213166fa&per_page=200
$membersFile = file_get_contents('https://www.strava.com/api/v3/clubs/193861/members?access_token=104e0a4a0f4c598bfa62446c6246f8f8213166fa&per_page=200');
$JSONObj = json_decode($membersFile);

// https://www.strava.com/api/v3/clubs/193861/activities?access_token=104e0a4a0f4c598bfa62446c6246f8f8213166fa&per_page=200
$activitiesFile = file_get_contents('https://www.strava.com/api/v3/clubs/193861/activities?access_token=104e0a4a0f4c598bfa62446c6246f8f8213166fa&per_page=200');
$JSONObjAct = json_decode($activitiesFile);

// Activiteiten
/*$curlActivities = curl_init();
curl_setopt ($curlActivities, CURLOPT_URL, "https://www.strava.com/api/v3/clubs/153253/activities?access_token=104e0a4a0f4c598bfa62446c6246f8f8213166fa&per_page=200");
curl_setopt($curlActivities, CURLOPT_RETURNTRANSFER, 1);
$result = curl_exec ($curlActivities);
curl_close ($curlActivities); 
$JSONObjAct = json_decode($result);*/

// Connect naar DB
$connection = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE);
//$connection = new mysqli($servername, $username, $password, $dbname);
if ($connection->connect_error) { die ("MySQL niet opgestart. Kalf. ". $connection->connect_error); }
// End connect DB
// END SETTINGS

// Leden in db djoefen.
foreach ($JSONObj as $key => $value) {
	// Eerst checken of "the member" al in de users tabel zit.
	// if not, awel, steekt ze er in.
	if ($result = $connection->query("SELECT id FROM leden WHERE id = " . $value->id)->num_rows == 0) {
		$sqlMembers = "INSERT INTO leden (id, lFname, lName, lStad, lProvincie, lSex) VALUES (" . $value->id . ",'" . addSlashes($value->firstname) . "','" . addSlashes($value->lastname) . "','" . $value->city . "','" . $value->state . "','" . $value->sex . "')";
		$connection->query($sqlMembers);
		echo $value->firstname . " " . $value->lastname . "<br>";
	}
}
// End Foreach ledenlijst

// foreach laatste 200 activiteiten leden WTC Surplatse
// Avg speed in m/s. Vermenigvuldigen met 3,6
foreach ($JSONObjAct as $keyAct => $valueAct) {
	// Checken of die activity er al in zit
	// if not, djoeft ze er in.
	if ($result = $connection->query("SELECT aID FROM activities WHERE aid = " . $valueAct->id)->num_rows == 0) {
		// Haal de weeknummer op
		$datumAct = DateTime::createFromFormat('Y-m-d\TH:i:s\Z', $valueAct->start_date_local); 
		$weeknr = $datumAct->format('W');
		$avgWatt = NULL;
		$deviceWatt = NULL;
		$maxHR = NULL;
		$lat = NULL;
		$lon = NULL;
		$rollen = NULL;
		$ww = NULL;
		// if false in JSON == blanco
		if ($valueAct->trainer == '' ) { $rollen = 0; } else { $rollen = 1;}
		if ($valueAct->commute == '' ) { $ww = 0; } else { $ww = 1;}
		// Watt toevoegen
		// Property enkel beschikbaar voor Rides
		if ($valueAct->type == 'Ride') {
			if (isset($valueAct->average_watts)) { $avgWatt = $valueAct->average_watts; }
			else { $avgWatt = 0; };
			if ($valueAct->device_watts) { 	$deviceWatt = $valueAct->device_watts; }
			else { $deviceWatt = 0;	}
		} else {		 
			$avgWatt = 'NULL';
			$deviceWatt = 'NULL';  
		}
		if (isset($valueAct->max_heartrate)) { $maxHR = $valueAct->max_heartrate; }
		else { $maxHR = 'NULL'; };
		if (isset ($valueAct->start_latitude)) { $lat = $valueAct->start_latitude; $lon = $valueAct->start_longitude;} else { $lat = 'NULL'; $lon = 'NULL';}
		// Eind weeknummer
		$sqlActivities = "INSERT INTO activities (athleetid, aID, aName, aDate, aDistance, aElevation, aType, aMovingtime, aElapsedtime, Weeknummer, aAvgSpeed, aGearID, aMaxSpeed, aAvgWatt, aDeviceWatts, aLat, aLong, aMaxHR, jaar, aPolyline, aPrs, aFotos, aRollen, aCommute) VALUES (" . $valueAct->athlete->id . ",'" . $valueAct->id . "','" . addslashes($valueAct->name) . "','" . $valueAct->start_date . "'," . $valueAct->distance . "," . $valueAct->total_elevation_gain . ",'" . $valueAct->type . "'," . $valueAct->moving_time .  "," . $valueAct->elapsed_time . "," . $weeknr . "," . $valueAct->average_speed . ",'" . $valueAct->gear_id . "'," . $valueAct->max_speed . "," . $avgWatt . "," . $deviceWatt . ", "  . $lat . ", " . $lon . ", " . $maxHR . ",2017,'" .  addslashes(addSlashes($valueAct->map->summary_polyline)) . "' ," . $valueAct->pr_count . ", " . $valueAct->total_photo_count ."," . $rollen . ", " . $ww . ")"; 
		echo $sqlActivities . "<br>";
		echo $valueAct->athlete->firstname . " " . $valueAct->athlete->lastname . " - " . addSlashes($valueAct->name). "<br>";
		//echo $sqlActivities . "<br>";
		$connection->query($sqlActivities);
	} 
	// ELSE EEN UPDATE! Alhoewel, titel boeit niet.
}
 echo "Databases populated.<br>";
?>
